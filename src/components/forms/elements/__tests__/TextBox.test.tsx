import * as React from "react";
import {shallow, ShallowWrapper } from "enzyme";
import {TextBox, TextBoxProps} from "../TextBox";

const VALID_TEXTPROPS: TextBoxProps = {
    altValue: "First Name",
    name: "fName",
    onBlur:() => {
        return;
    },
    passVal: "",
    type: "text",
    value: "George Roots",
};

let wrapper: ShallowWrapper<{}, {}>
beforeAll(() => {
    wrapper = shallow(
        <TextBox {...VALID_TEXTPROPS} />
    )
});

describe("Component TextBox", () => {
    it("should render", () => {
        expect(wrapper).toMatchSnapshot();
    });
});