import * as React from "react";

export const DumbButton = (props: any) => {
    return (
        <input className="btn"
               name={props.name}
               style={{backgroundColor: "#00A1DF", color: "#ffffff"}}
               type="button"
               value={props.value} />
    );
}