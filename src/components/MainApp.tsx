import * as React from "react";
import {Link} from "react-router-dom";
export const MainApp = () => {
  return (
      <div className="container">
        <div classID="jumbotron">
            <div className="container">
                <h1 className="display-3">Web Starter</h1>
                <p>This is a template for a simple marketing or informational website. It includes a large callout
                called a jumbotron and three supporting pieces of content. Use it as a starting point to create
                something more unique.</p>
                <p>
                    <Link className="btn btn-primary btn-lg" to="/partnering" role="button">Learn more »</Link>
                </p>
            </div>
        </div>
        <div className="CTA row">
          <div className="col-md-4">About Page</div>
          <div className="col-md-4">Info Page</div>
          <div className="col-md-4"><Link to="/partnering">Become a Partner</Link></div>
        </div>
        <div className="col-md-12">More site verbiage</div>
      </div>
  )
}