import * as React from "react";
import { BrowserRouter as Router, Route} from "react-router-dom";
import {MainApp} from "./MainApp";
import {Partnerships} from "./forms/Partnerships";
import {Nav} from "./Nav";

export const SiteRoutes = () => {
    return (
        <div>
            <Router>
                <div>
                    <Nav />
                    <Route exact path="/" component={MainApp} />
                    <Route path="/partnering" component={Partnerships} />
                </div>
            </Router>
        </div>
    );
};