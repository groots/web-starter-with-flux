import * as React from "react";
import {ComboBox} from "../../../components/forms/elements/ComboBox";

interface MultiSelectBoxProps {
    name: string;
    validate: boolean;
    altValue: string;
}

export class MultiSelectBox extends React.Component<MultiSelectBoxProps, {}> {
    render() {
        return (
            <ComboBox name={this.props.name} validate={true} altName={this.props.altValue}  />
        );
    }
}